package Model;

import Controlador.ControladorBoto;
import Network.ServerComunication;
import Vista.Finestra;

import javax.swing.*;


/**
 * Created by Uri on 31/3/17.
 */
public class main {

    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable() {
          //  @Override
            public void run() {
                Finestra vista = new Finestra();
                vista.setVisible(true);

                ServerComunication serverComunication = new ServerComunication(vista);
                serverComunication.startServerComunication();

                ControladorBoto controlador = new ControladorBoto(vista, serverComunication);

                vista.assingaControladorBoto(controlador);
                vista.posaControlador(controlador);
            }
        });
    }
}
