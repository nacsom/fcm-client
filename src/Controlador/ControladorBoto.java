package Controlador;

import Model.Billete;
import Model.Vuelo;
import Network.ServerComunication;
import Vista.Aeroport;
import Vista.DateTimePicker;
import Vista.Finestra;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;

/**
 * Created by Uri on 14/4/2017.
 */
public class ControladorBoto implements ActionListener {

    private Finestra vista;

    private ServerComunication serverComunication;

    private DateTimePicker dateTimePicker;

    public ControladorBoto(Finestra vista, ServerComunication serverComunication) {
        this.serverComunication = serverComunication;
        this.dateTimePicker = vista.getCompraBitllet().getDateTimePicker();
        this.vista = vista;
    }


    @Override

    public void actionPerformed (ActionEvent e) {

        if (e.getActionCommand().equals("Entrar")) {
            vista.vesLogin();
        }
        if(e.getActionCommand().equals("Cancela")){
            vista.vesMenu();
        }
        if (e.getActionCommand().equals("Continua")){
            vista.vesRegistrarse();
        }
        if(e.getActionCommand().equals("Entra")){   //BOTO DE LOGIN
            vista.comprovaLogin();
            if(serverComunication.wantToLogin())  vista.vesMenu2();     //Si ha loggejat correctament, obre el menu, sino no.
        }
        if(e.getActionCommand().equals("Login")){
            vista.vesLogin();
        }
        if (e.getActionCommand().equals("Registrar-se")){
            vista.vesRegistrarse();
        }
        if (e.getActionCommand().equals("Desconnectar-se")){
            serverComunication.wantToDisconnect();
            vista.vesMenu();
        }
        if (e.getActionCommand().equals("Consultar aeroports")){
            try {
                vista.getAeroport().setJcbAeropuertos(serverComunication.airportGetAvailableAirports());   //Quan es fa petició d'entrada, s'actualitzen els aeroports
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            }
            vista.vesAeroport();
        }
        if (e.getActionCommand().equals("CONSULTA_10")){
            vista.consulta10(serverComunication.want10Flights());
        }
        if(e.getActionCommand().equals("Consulta bitllets")){
        }
        if(e.getActionCommand().equals("Consulta vols")){
            if(vista.getCompraBitllet().checkCamposSeleccionados()) {
                vista.getCompraBitllet().muestraVuelosFecha(serverComunication.wantToBuyBilleteShowThem(vista.getCompraBitllet().getJcbAeropuertoOrigen().getSelectedItem(),
                        vista.getCompraBitllet().getJcbAeropuertoDestino().getSelectedItem(), vista.getCompraBitllet().getDateTimePicker().getDate()));
            }else{
                JOptionPane.showMessageDialog(vista,
                        "Has de seleccionar una data de sortida!",
                        "Error de selecció",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
        if(e.getActionCommand().equals("Registra't")){
            if(vista.comprovaRegistre()) serverComunication.wantToRegister();
        }
        if(e.getActionCommand().equals("Cancela aeroports")){
            vista.vesMenu2();
        }
        if(e.getActionCommand().equals("compraBitllet Cancela")){
            vista.vesMenu2();
        }
        if(e.getActionCommand().equals("Compra billete")){
            String referencia = vista.getCompraBitllet().getBilleteSeleccionado();
            if(referencia == null){
                JOptionPane.showMessageDialog(vista,
                        "Has de seleccionar algun bittlet en el camp!",
                        "Error de selecció",
                        JOptionPane.ERROR_MESSAGE);
            }else {
                //Enviem bittllet per comprovar si està bé
                String s = serverComunication.wantToBuyBillete(referencia, vista.getLogin().getUsername(), 1);
                if (s.equals("OK")) {
                    //BITLLET COMPRAT OK
                    JOptionPane.showMessageDialog(vista,
                            "El bitllet s'ha comprat satisfactòriament!",
                            "Compra correcta",
                            JOptionPane.WARNING_MESSAGE);
                    vista.consultaBilletesUsuario(serverComunication.wantBilletesUser());   //Actualitzem pantalla de bitllets

                } else if (s.equals("KO")) {
                    //BITLLET NO COMPRAT, ERROR
                    JOptionPane.showMessageDialog(vista,
                            "No s'ha pogut comprar el bitllet seleccionat..",
                            "Error de compra",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        if(e.getActionCommand().equals("Comprar i visualitzar bitllets")){
            try {
                vista.getCompraBitllet().setJcbAeropuertoOrigen(serverComunication.airportGetAvailableAirports());  //Actualitzem els aeroports
                vista.consultaBilletesUsuario(serverComunication.wantBilletesUser());
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            }
            vista.vesCompraBitllet();
        }
        if(e.getActionCommand().equals("Veure estat dels vols")){
            vista.vesEstatVols();
            vista.getEstatVols().muestraBilletesUsuario(serverComunication.wantBilletesUser());
        }
        if(e.getActionCommand().equals("CONSULTA_ESTADO")){
            vista.getEstatVols().muestraEstadoVuelo();
            vista.getEstatVols().muestraEstadoVuelo(serverComunication.wantShowFlightStatus());
        }
        if(e.getActionCommand().equals("TIRA_ATRAS")){
            vista.getEstatVols().muestraEstadoVuelo();
        }
        if(e.getActionCommand().equals("Cancela Estat Vols")){
            vista.vesMenu2();
        }
    }
}



