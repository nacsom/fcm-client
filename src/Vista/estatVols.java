package Vista;

import Model.Billete;
import Model.Vuelo;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.LinkedList;

/**
 * Created by Toni on 06/05/2017.
 */
public class estatVols extends  JPanel{
    private JTable jtVuelosComprados;   //PANELL 2
    private DefaultTableModel modelVuelosComprados;
    private JTable jtEstadoVuelos;
    private DefaultTableModel modelEstadoVuelos;
    private CardLayout cards;
    private JPanel cardPanel;
    private JButton jbConsultaEstado;
    private JButton jbCancelarEstado;
    private JButton jbCancela;

    public estatVols(){
        jbConsultaEstado = new JButton("Consulta estat");
        jbConsultaEstado.setActionCommand("CONSULTA_ESTADO");

        jbCancelarEstado = new JButton("Ves enrere");
        jbCancelarEstado.setActionCommand("TIRA_ATRAS");
        cards = new CardLayout();

        jbCancela = new JButton("Cancela");
        jbCancela.setActionCommand("Cancela Estat Vols");

        modelVuelosComprados = new DefaultTableModel();
        jtVuelosComprados = new JTable(modelVuelosComprados);       //Panel 1
        modelVuelosComprados.addColumn("Referencia");
        modelVuelosComprados.addRow(new Object[]{"Referencia"});

        modelEstadoVuelos = new DefaultTableModel();
        jtEstadoVuelos = new JTable(modelEstadoVuelos);
        modelEstadoVuelos.addColumn("Referencia");
        modelEstadoVuelos.addColumn("Data Sortida");
        modelEstadoVuelos.addColumn("Duració");
        modelEstadoVuelos.addColumn("Retràs");
        modelEstadoVuelos.addColumn("Referècia Avió");
        modelEstadoVuelos.addColumn("Referencia Origen");
        modelEstadoVuelos.addColumn("Referencia Desti");
        modelEstadoVuelos.addColumn("Estat");
        modelEstadoVuelos.addRow(new Object[]{"Referència", "Data Sortida", "Duració", "Retràs", "Refèrencia Avió",
                "Referència Origen", "Refèrencia Destí", "Estat"});
        //_____________


        cardPanel = new JPanel();
        cardPanel.setLayout(cards);
        cards.show(cardPanel, "Vols");

        JPanel firstCard = new JPanel(new BorderLayout());
        firstCard.setBackground(Color.GREEN);

        JPanel secondCard = new JPanel(new BorderLayout());
        secondCard.setBackground(Color.BLUE);

        cardPanel.add(firstCard, "Vols");
        cardPanel.add(secondCard, "Estado vuelos");

        JPanel aux = new JPanel(new GridLayout(2,0));
        aux.add(jbConsultaEstado);
        aux.add(jbCancela);

        firstCard.add(jtVuelosComprados, BorderLayout.CENTER);
        firstCard.add(aux, BorderLayout.SOUTH);
        secondCard.add(jtEstadoVuelos, BorderLayout.CENTER);
        secondCard.add(jbCancelarEstado, BorderLayout.SOUTH);

        this.setLayout(new BorderLayout());
        this.add(cardPanel, BorderLayout.CENTER);
    }

    public void muestraBilletesUsuario(LinkedList<Billete> billetes){
        int numeroBilletes = billetes.size();
        modelVuelosComprados.setRowCount(0);
        modelVuelosComprados.setColumnCount(0);
        modelVuelosComprados.addColumn("Referencia");
        modelVuelosComprados.addRow(new Object[]{"Referencia"});
        for( int i = 0; i < numeroBilletes; i++){
            modelVuelosComprados.addRow(new Object[]{billetes.get(i).getReferencia()});
        }
    }

    public void muestraEstadoVuelo(Vuelo vuelo){
        modelEstadoVuelos.setRowCount(0);
        modelEstadoVuelos.setColumnCount(0);
        modelEstadoVuelos.addColumn("Referencia");
        modelEstadoVuelos.addColumn("Data Sortida");
        modelEstadoVuelos.addColumn("Duració");
        modelEstadoVuelos.addColumn("Retràs");
        modelEstadoVuelos.addColumn("Referècia Avió");
        modelEstadoVuelos.addColumn("Referencia Origen");
        modelEstadoVuelos.addColumn("Referencia Desti");
        modelEstadoVuelos.addColumn("Estat");
        modelEstadoVuelos.addRow(new Object[]{"Referència", "Data Sortida", "Duració", "Retràs", "Refèrencia Avió",
                "Referència Origen", "Refèrencia Destí", "Estat"});
        modelEstadoVuelos.addRow(new Object[]{vuelo.getReferencia(), vuelo.getFechaSalida(), vuelo.getDuracion(), vuelo.getRetraso(),
                                 vuelo.getReferenciaAvion(), vuelo.getReferenciaOrigen(), vuelo.getGetReferenciaDestina(), vuelo.getEstado()});

    }

    public void muestraEstadoVuelo(){
        cards.next(cardPanel);
    }

    public JTable getJtVuelosComprados() {
        return jtVuelosComprados;
    }

    public void setJtVuelosComprados(JTable jtVuelosComprados) {
        this.jtVuelosComprados = jtVuelosComprados;
    }

    public DefaultTableModel getModelVuelosComprados() {
        return modelVuelosComprados;
    }

    public void setModelVuelosComprados(DefaultTableModel modelVuelosComprados) {
        this.modelVuelosComprados = modelVuelosComprados;
    }

    public JTable getJtEstadoVuelos() {
        return jtEstadoVuelos;
    }

    public void setJtEstadoVuelos(JTable jtEstadoVuelos) {
        this.jtEstadoVuelos = jtEstadoVuelos;
    }

    public DefaultTableModel getModelEstadoVuelos() {
        return modelEstadoVuelos;
    }

    public void setModelEstadoVuelos(DefaultTableModel modelEstadoVuelos) {
        this.modelEstadoVuelos = modelEstadoVuelos;
    }

    public CardLayout getCards() {
        return cards;
    }

    public void setCards(CardLayout cards) {
        this.cards = cards;
    }

    public JPanel getCardPanel() {
        return cardPanel;
    }

    public void setCardPanel(JPanel cardPanel) {
        this.cardPanel = cardPanel;
    }

    public JButton getJbConsultaEstado() {
        return jbConsultaEstado;
    }

    public void setJbConsultaEstado(JButton jbConsultaEstado) {
        this.jbConsultaEstado = jbConsultaEstado;
    }

    public JButton getJbCancelarEstado() {
        return jbCancelarEstado;
    }

    public void setJbCancelarEstado(JButton jbCancelarEstado) {
        this.jbCancelarEstado = jbCancelarEstado;
    }

    public JButton getJbCancela() {
        return jbCancela;
    }

    public void setJbCancela(JButton jbCancela) {
        this.jbCancela = jbCancela;
    }
}
