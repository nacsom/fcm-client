package Vista;

import javax.swing.*;
import java.awt.*;


/**
 * Created by Sefa on 5/5/17.
 */
public class Menu2 extends JPanel{

    private JButton jbVisvols;
    private JButton jbVeureVols;
    private JButton jbVisAeroports;
    private JButton jbDesconectar;
    private JLabel jlEspai;
    private JPanel jpCentre1;
    private JPanel jpCentre;
    private JLabel jlTitol;

    public Menu2(){


        jlTitol = new JLabel(new ImageIcon("Imatges/titol.png"));
        jlTitol.setHorizontalAlignment(JLabel.CENTER);
        jlTitol.setAlignmentX(Component.CENTER_ALIGNMENT);
        jlTitol.setBackground(Color.white);

        jpCentre = new JPanel();
        jpCentre.setLayout(new BoxLayout(jpCentre, BoxLayout.PAGE_AXIS));
        jpCentre.setBackground(Color.white);

        jbVisvols = new JButton("Comprar i visualitzar bitllets");
        jbVisvols.setAlignmentX(Component.CENTER_ALIGNMENT);
        jbVisvols.setForeground(Color.white);
        jbVisvols.setIcon(new ImageIcon("Imatges/Comprar Bitllets.png"));
        jbVisvols.setFocusPainted(false);
        jbVisvols.setBackground(Color.white);
        jbVisvols.setBorderPainted(false);
        jbVisvols.setPreferredSize(new Dimension(1000,100));
        jbVisvols.setRolloverIcon(new ImageIcon("Imatges/Comprar Bitllets2.png"));
        jbVisvols.setPressedIcon(new ImageIcon("Imatges/Comprar Bitllets2.png"));
        jbVisvols.setFont(new Font ("Arial",Font.PLAIN,0));


       jlEspai = new JLabel("                              ");


        jbVisAeroports = new JButton("Consultar aeroports");
        jbVisAeroports.setAlignmentX(Component.CENTER_ALIGNMENT);
        jbVisAeroports.setForeground(Color.white);
        jbVisAeroports.setIcon(new ImageIcon("Imatges/Consultar Aeroports.png"));
        jbVisAeroports.setFocusPainted(false);
        jbVisAeroports.setBackground(Color.white);
        jbVisAeroports.setBorderPainted(false);
        jbVisAeroports.setPreferredSize(new Dimension(1000,100));
        jbVisAeroports.setRolloverIcon(new ImageIcon("Imatges/Consultar Aeroports2.png"));
        jbVisAeroports.setPressedIcon(new ImageIcon("Imatges/Consultar Aeroports2.png"));
        jbVisAeroports.setFont(new Font ("Arial",Font.PLAIN,0));

        jbVeureVols = new JButton("Veure estat dels vols");
        jbVeureVols.setAlignmentX(Component.CENTER_ALIGNMENT);
        jbVeureVols.setForeground(Color.white);
        jbVeureVols.setIcon(new ImageIcon("Imatges/Veure Vols.png"));
        jbVeureVols.setFocusPainted(false);
        jbVeureVols.setBackground(Color.white);
        jbVeureVols.setBorderPainted(false);
        jbVeureVols.setPreferredSize(new Dimension(1000,100));
        jbVeureVols.setRolloverIcon(new ImageIcon("Imatges/Veure Vols2.png"));
        jbVeureVols.setPressedIcon(new ImageIcon("Imatges/Veure Vols2.png"));
        jbVeureVols.setFont(new Font ("Arial",Font.PLAIN,0));


        jbDesconectar = new JButton("Desconnectar-se");
        jbDesconectar.setAlignmentX(Component.CENTER_ALIGNMENT);



        jpCentre.add(jbVisvols);
        jpCentre.add(jlEspai);
        jpCentre.add(jbVisAeroports);
        jpCentre.add(jlEspai);
        jpCentre.add(jbVeureVols);
        jpCentre.add(jlEspai);
        jpCentre.add(jbVeureVols);
        jpCentre.add(jlEspai);
        jpCentre.add(jbDesconectar);

        this.setLayout(new BorderLayout());
        this.add(jlTitol, BorderLayout.PAGE_START);
        this.add(jpCentre, BorderLayout.CENTER);
        this.setBackground(Color.white);

    }

    public JButton getJbVisvols() {
        return jbVisvols;
    }

    public void setJbVisvols(JButton jbVisvols) {
        this.jbVisvols = jbVisvols;
    }

    public JButton getJbVeureVols() {
        return jbVeureVols;
    }

    public void setJbVeureVols(JButton jbVeureVols) {
        this.jbVeureVols = jbVeureVols;
    }

    public JButton getJbVisAeroports() {
        return jbVisAeroports;
    }

    public void setJbVisAeroports(JButton jbVisAeroports) {
        this.jbVisAeroports = jbVisAeroports;
    }

    public JPanel getJpCentre() {
        return jpCentre;
    }

    public void setJpCentre(JPanel jpCentre) {
        this.jpCentre = jpCentre;
    }

    public JButton getJbDesconectar() {
        return jbDesconectar;
    }

    public void setJbDesconectar(JButton jbDesconectar) {
        this.jbDesconectar = jbDesconectar;
    }

    public JLabel getJlEspai() {
        return jlEspai;
    }

    public void setJlEspai(JLabel jlEspai) {
        this.jlEspai = jlEspai;
    }

    public JPanel getJpCentre1() {
        return jpCentre1;
    }

    public void setJpCentre1(JPanel jpCentre1) {
        this.jpCentre1 = jpCentre1;
    }

    public JLabel getJlTitol() {
        return jlTitol;
    }

    public void setJlTitol(JLabel jlTitol) {
        this.jlTitol = jlTitol;
    }

}
