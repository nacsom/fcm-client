
package Vista;

import Model.Billete;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

import Model.Vuelo;
import org.jdesktop.swingx.JXDatePicker;


/**
 * Created by Sefa on 28/4/17.
 */
public class CompraBitllet extends  JPanel{

    private DefaultTableModel modelBilletes;  //PANELL 1
    private DefaultTableModel modelVuelosFecha;
    private JButton jbConsultaBilletes;
    private JButton jbCompraBillete;
    private JButton jbCancela;
    private  JTable jtVuelosFecha;
    private JTable jtBilletes;
    private JComboBox<String> jcbAeropuertoOrigen;
    private JComboBox<String> jcbAeropuertoDestino;

    private Date jtfFecha;
    private DateTimePicker dateTimePicker;


    public CompraBitllet(){


        Calendar cal = Calendar.getInstance();

        dateTimePicker = new DateTimePicker();
        dateTimePicker.setFormats( DateFormat.getDateTimeInstance( DateFormat.SHORT, DateFormat.MEDIUM ) );
        dateTimePicker.setTimeFormat( DateFormat.getTimeInstance( DateFormat.MEDIUM ) );
        dateTimePicker.setDate(cal.getTime());
        //dateTimePicker.setDate(date);
        jtfFecha = dateTimePicker.getDate();


        JPanel panel = new JPanel(new BorderLayout());
        Panel panelNorth = new Panel(new GridLayout(3,1));
        Panel panelCenter = new Panel(new GridLayout(2,1));

        jtVuelosFecha = new JTable();
        modelVuelosFecha = new DefaultTableModel();
        jtVuelosFecha = new JTable(modelVuelosFecha);   //Taula Vols

        jtBilletes = new JTable();
        modelBilletes = new DefaultTableModel();
        jtBilletes = new JTable(modelBilletes);     //Taula bitllets usuari

        jcbAeropuertoOrigen = new JComboBox();
        jcbAeropuertoDestino = new JComboBox();

        modelVuelosFecha.addColumn("Referencia");
        modelVuelosFecha.addColumn("Data Sortida");
        modelVuelosFecha.addColumn("Duració");
        modelVuelosFecha.addColumn("Referència Origen");
        modelVuelosFecha.addColumn("Referència Destí");
        //modelVuelosFecha.addRow(new Object[]{"Referencia", "Data Sortida", "Duracio", "Referencia Origen", "Referencia Desti"});


        modelBilletes.addColumn("Referència");
        modelBilletes.addColumn("Refència Usuari");
        modelBilletes.addColumn("Referència Vol");
        //modelBilletes.addRow(new Object[]{"Referencia", "Referencia Usuari", "Referencia Vol"});

        JScrollPane scrollPaneVuelosFecha = new JScrollPane(jtVuelosFecha);
        JScrollPane scrollPaneBilletes = new JScrollPane(jtBilletes);
        panelCenter.add(scrollPaneVuelosFecha);
        panelCenter.add(scrollPaneBilletes);

        jbConsultaBilletes = new JButton("Consulta vols");
        jbConsultaBilletes.setActionCommand("Consulta vols");
        jbCompraBillete = new JButton("Compra billete");
        jbCompraBillete.setActionCommand("Compra billete");
        jbCancela = new JButton("Cancela");
        jbCancela.setActionCommand("compraBitllet Cancela");

        JPanel aux = new JPanel(new GridLayout(3,0));
        aux.add(jbConsultaBilletes);
        aux.add(jbCompraBillete);
        aux.add(jbCancela);
        panel.add(aux, BorderLayout.NORTH);
        panel.add(panelCenter, BorderLayout.CENTER);
        panel.add(panelNorth, BorderLayout.SOUTH);

        this.setLayout(new BorderLayout());
        this.add(panel, BorderLayout.CENTER);

        panelNorth.add(jcbAeropuertoOrigen);
        panelNorth.add(jcbAeropuertoDestino);
        panelNorth.add(dateTimePicker);
    }

    public void muestraBilletesUsuario(LinkedList<Billete> billetes){
        int numeroBilletes = billetes.size();
        modelBilletes.setRowCount(0);
        modelBilletes.setColumnCount(0);
        modelBilletes.addColumn("Referencia");
        modelBilletes.addColumn("Referencia Usuari");
        modelBilletes.addColumn("Referencia Vol");
        //modelBilletes.addRow(new Object[]{"Referencia", "Referencia Usuari", "Referencia Vol"});
        for( int i = 0; i < numeroBilletes; i++){
            modelBilletes.addRow(new Object[]{billetes.get(i).getReferencia(), billetes.get(i).getReferenciaUsuario(),
                    billetes.get(i).getReferenciaVuelo()});

        }
    }

    public void muestraVuelosFecha(LinkedList<Vuelo> vuelos){
        int numeroVuelos = vuelos.size();
        modelVuelosFecha.setRowCount(0);
        modelVuelosFecha.setColumnCount(0);
        modelVuelosFecha.addColumn("Referencia");
        modelVuelosFecha.addColumn("Data Sortida");
        modelVuelosFecha.addColumn("Duració");
        modelVuelosFecha.addColumn("Referència Origen");
        modelVuelosFecha.addColumn("Referència Destí");
        //modelVuelosFecha.addRow(new Object[]{"Referencia", "Data Sortida", "Duracio", "Referencia Origen", "Referencia Desti"});
        for( int i = 0; i < numeroVuelos; i++){
            modelVuelosFecha.addRow(new Object[]{vuelos.get(i).getReferencia(), vuelos.get(i).getFechaSalida(), vuelos.get(i).getDuracion(),
                    vuelos.get(i).getReferenciaOrigen(), vuelos.get(i).getGetReferenciaDestina()});

        }
    }

    public DefaultTableModel getModelBilletes() {
        return modelBilletes;
    }

    public void setModelBilletes(DefaultTableModel modelBilletes) {
        this.modelBilletes = modelBilletes;
    }

    public JButton getJbConsultaBilletes() {
        return jbConsultaBilletes;
    }

    public void setJbConsultaBilletes(JButton jbConsultaBilletes) {
        this.jbConsultaBilletes = jbConsultaBilletes;
    }

    public JTable getJtBilletes() {
        return jtBilletes;
    }

    public void setJtBilletes(JTable jtBilletes) {
        this.jtBilletes = jtBilletes;
    }

    public DefaultTableModel getModelVuelosFecha() {
        return modelVuelosFecha;
    }

    public void setModelVuelosFecha(DefaultTableModel modelVuelosFecha) {
        this.modelVuelosFecha = modelVuelosFecha;
    }

    public JButton getJbCancela() {
        return jbCancela;
    }

    public void setJbCancela(JButton jbCancela) {
        this.jbCancela = jbCancela;
    }

    public JTable getJtVuelosFecha() {
        return jtVuelosFecha;
    }

    public void setJtVuelosFecha(JTable jtVuelosFecha) {
        this.jtVuelosFecha = jtVuelosFecha;
    }

    public JComboBox<String> getJcbAeropuertoOrigen() {
        return jcbAeropuertoOrigen;
    }

    public void setJcbAeropuertoOrigen(LinkedList<String> airportsName) {
        int numberOfAirports = airportsName.size();
        int numberOfComboBox = this.jcbAeropuertoOrigen.getItemCount();
        for(int i = 0; i < numberOfAirports; i++){
            boolean dontAdd = false;
            for(int j = 0; j < numberOfComboBox; j++){
                if(this.jcbAeropuertoOrigen.getItemAt(j).equals(airportsName.get(i))) dontAdd = true;
            }
            if(!dontAdd) this.jcbAeropuertoOrigen.addItem(airportsName.get(i));
        }
        setJcbAeropuertoDestino(airportsName);  //Fem el mateix amb els de destí (no cal fer petició per duplicat al servidor)
    }

    public JComboBox<String> getJcbAeropuertoDestino() {
        return jcbAeropuertoDestino;
    }

    public void setJcbAeropuertoDestino(LinkedList<String> airportsName) {
        int numberOfAirports = airportsName.size();
        int numberOfComboBox = this.jcbAeropuertoDestino.getItemCount();
        for(int i = 0; i < numberOfAirports; i++){
            boolean dontAdd = false;
            for(int j = 0; j < numberOfComboBox; j++){
                if(this.jcbAeropuertoDestino.getItemAt(j).equals(airportsName.get(i))) dontAdd = true;
            }
            if(!dontAdd) this.jcbAeropuertoDestino.addItem(airportsName.get(i));
        }
    }

    public String getBilleteSeleccionado(){
        int row = jtVuelosFecha.getSelectedRow();
        if(row == -1) return null;
        String s = (String) modelVuelosFecha.getValueAt(row, 0);    //Retornem només la referència del vol
        return s;
    }

    public boolean checkCamposSeleccionados(){
        boolean ok = true;
        //if(jtfFecha.equals(null)) ok = false;
        return ok;
    }

    public void setJcbAeropuertoOrigen(JComboBox<String> jcbAeropuertoOrigen) {
        this.jcbAeropuertoOrigen = jcbAeropuertoOrigen;
    }

    public void setJcbAeropuertoDestino(JComboBox<String> jcbAeropuertoDestino) {
        this.jcbAeropuertoDestino = jcbAeropuertoDestino;
    }

    public Date getJtfFecha() {
        return jtfFecha;
    }

    public void setJtfFecha(Date jtfFecha) {
        this.jtfFecha = jtfFecha;
    }

    public DateTimePicker getDateTimePicker() {
        return dateTimePicker;
    }

    public void setDateTimePicker(DateTimePicker dateTimePicker) {
        this.dateTimePicker = dateTimePicker;
    }

    public JButton getJbCompraBillete() {
        return jbCompraBillete;
    }

    public void setJbCompraBillete(JButton jbCompraBillete) {
        this.jbCompraBillete = jbCompraBillete;
    }
}
