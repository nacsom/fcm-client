package Vista;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Sefa on 31/3/17.
 */
public class Login extends JPanel{

    //elements grafics de la GUI del login/Registre
    private JLabel jlUsuari;
    private JLabel jlPassword;
    private JButton jbEntra;
    private JButton jbCancela;
    private JTextField jtfUsuari;
    private JPasswordField jpfPassword;
    private JPanel jpUsuari;
    private JPanel jpPassword;
    private JPanel jpBotons;
    private JPanel jpPrincipal;
    private JPanel jpInici;
    private JPanel jpFinal;
    private JLabel jlInfo;

    private String username;

    public Login(){

        jlInfo = new JLabel(new ImageIcon("Imatges/ExplicacioLogin.png"));

        jlUsuari = new JLabel("Usuari");
        jtfUsuari = new JTextField();
        jtfUsuari.setPreferredSize(new Dimension(100, 25));

        jlPassword = new JLabel("Contrasenya");
        jpfPassword = new JPasswordField();
        jpfPassword.setPreferredSize(new Dimension(100, 25));

        jbEntra = new JButton("Entra");



        jbCancela = new JButton("Cancela");

        jpUsuari = new JPanel();
        jpUsuari.add(jlUsuari);
        jpUsuari.add(jtfUsuari);
        jpUsuari.setBackground(Color.white);

        jpPassword = new JPanel();
        jpPassword.add(jlPassword);
        jpPassword.add(jpfPassword);
        jpPassword.setBackground(Color.white);

        jpBotons = new JPanel(new FlowLayout(FlowLayout.CENTER));
        jpBotons.add(jbEntra);
        jpBotons.add(jbCancela);
        jpBotons.setBackground(Color.white);

        jpPrincipal = new JPanel(new GridLayout(4,1));
        jpPrincipal.add(jlInfo);
        jpPrincipal.add(jpUsuari);
        jpPrincipal.add(jpPassword);
        jpPrincipal.add(jpBotons);
        jpPrincipal.setBackground(Color.white);


        jpInici = new JPanel();
        jlInfo.setAlignmentX(Component.CENTER_ALIGNMENT);

        jpInici.add(jlInfo);
        jpInici.setBackground(Color.white);

        jpFinal = new JPanel();
        jpFinal.setBackground(Color.white);

        this.setLayout(new BorderLayout());
        this.add(jpInici,BorderLayout.PAGE_START);
        this.add(jpPrincipal, BorderLayout.CENTER);
        this.add(jpFinal,BorderLayout.PAGE_END);
        this.setBackground(Color.white);



        setSize(825, 500);



    }

    public JLabel getJlUsuari() {
        return jlUsuari;
    }

    public void setJlUsuari(JLabel jlUsuari) {
        this.jlUsuari = jlUsuari;
    }

    public JLabel getJlPassword() {
        return jlPassword;
    }

    public void setJlPassword(JLabel jlPassword) {
        this.jlPassword = jlPassword;
    }

    public JButton getJbEntra() {
        return jbEntra;
    }

    public void setJbEntra(JButton jbEntra) {
        this.jbEntra = jbEntra;
    }

    public JButton getJbCancela() {
        return jbCancela;
    }

    public void setJbCancela(JButton jbCancela) {
        this.jbCancela = jbCancela;
    }

    public JTextField getJtfUsuari() {
        return jtfUsuari;
    }

    public void setJtfUsuari(JTextField jtfUsuari) {
        this.jtfUsuari = jtfUsuari;
    }

    public JPasswordField getJpfPassword() {
        return jpfPassword;
    }

    public void setJpfPassword(JPasswordField jpfPassword) {
        this.jpfPassword = jpfPassword;
    }

    public JPanel getJpUsuari() {
        return jpUsuari;
    }

    public void setJpUsuari(JPanel jpUsuari) {
        this.jpUsuari = jpUsuari;
    }

    public JPanel getJpPassword() {
        return jpPassword;
    }

    public void setJpPassword(JPanel jpPassword) {
        this.jpPassword = jpPassword;
    }

    public JPanel getJpBotons() {
        return jpBotons;
    }

    public void setJpBotons(JPanel jpBotons) {
        this.jpBotons = jpBotons;
    }

    public JPanel getJpPrincipal() {
        return jpPrincipal;
    }

    public void setJpPrincipal(JPanel jpPrincipal) {
        this.jpPrincipal = jpPrincipal;
    }

    public JPanel getJpInici() {
        return jpInici;
    }

    public void setJpInici(JPanel jpInici) {
        this.jpInici = jpInici;
    }

    public JPanel getJpFinal() {
        return jpFinal;
    }

    public void setJpFinal(JPanel jpFinal) {
        this.jpFinal = jpFinal;
    }

    public JLabel getJlInfo() {
        return jlInfo;
    }

    public void setJlInfo(JLabel jlInfo) {
        this.jlInfo = jlInfo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
