package Vista;

import javax.swing.*;
import java.awt.*;


/**
 * Created by Uri on 6/4/2017.
 */
public class Registrarse extends JPanel{

    private JLabel jlEtiqueta;
    private JLabel jlRestriccions1;
    private JLabel jlRestriccions2;
    private JLabel jlRestriccions3;
    private JLabel jlRestriccions4;
    private JLabel jlRestriccions5;
    private JLabel jlRestriccions6;
    private JButton jbRegistre;
    private JButton jbCancela;
    private JLabel jlUsuari;
    private JLabel jlNom;
    private JLabel jlApellido;
    private JTextField jtfNom;
    private JTextField jtfApellido;
    private JTextField jtfUsuari;
    private JTextField jtfCorreu;
    private JPasswordField jpfContrasenya;
    private JPasswordField jpfConfirmar;
    private JLabel jlCorreu;
    private JLabel jlContrasenya;
    private JLabel jlConfirmar;
    private JPanel jpCenter;
    private JPanel jpBotons;
    private JPanel jpCenter1;
    private JPanel jpCenter2;
    private JPanel jpCenter3;
    private JPanel jpCenter4;
    private JPanel jpCenter5;
    private JPanel jpCenter6;
    private JPanel jpRestriccions;

    public Registrarse(){

        //Imatge del títol
        jlEtiqueta = new JLabel(new ImageIcon("Imatges/Registrar-se3.png"));
        jlEtiqueta.setHorizontalAlignment(JLabel.CENTER);
        jlEtiqueta.setAlignmentX(Component.CENTER_ALIGNMENT);


        //JLabel del usuari
        jlUsuari = new JLabel("Usuario");
        jlUsuari.setHorizontalAlignment(JLabel.CENTER);

        //JTextField del usuari
        jtfUsuari = new JTextField();
        jtfUsuari.setHorizontalAlignment(JTextField.LEFT);
        jtfUsuari.setAlignmentX(Component.RIGHT_ALIGNMENT);
        jtfUsuari.setPreferredSize(new Dimension(180, 20));

        //JLabel del mom
        jlNom = new JLabel("Nombre");
        jlNom.setHorizontalAlignment(JLabel.CENTER);

        //JTextField del NOM
        jtfNom = new JTextField();
        jtfNom.setHorizontalAlignment(JTextField.LEFT);
        jtfNom.setAlignmentX(Component.RIGHT_ALIGNMENT);
        jtfNom.setPreferredSize(new Dimension(180, 20));

        //JLabel del apellido
        jlApellido = new JLabel("Apellido");
        jlApellido.setHorizontalAlignment(JLabel.CENTER);

        //JTextField del APELLIDO
        jtfApellido = new JTextField();
        jtfApellido.setHorizontalAlignment(JTextField.LEFT);
        jtfApellido.setAlignmentX(Component.RIGHT_ALIGNMENT);
        jtfApellido.setPreferredSize(new Dimension(180, 20));



        //JLabel del correu
        jlCorreu = new JLabel("Email");
        jlCorreu.setHorizontalAlignment(JLabel.CENTER);

        //JTextField del correu
        jtfCorreu = new JTextField();
        jtfCorreu.setHorizontalAlignment(JTextField.LEFT);
        jtfCorreu.setAlignmentX(Component.CENTER_ALIGNMENT);
        jtfCorreu.setPreferredSize(new Dimension(180, 20));

        //JLabel de la contrasenya
        jlContrasenya = new JLabel("Contraseña");
        jlContrasenya.setHorizontalAlignment(JLabel.CENTER);

        //JPasswordField de constrasenya
        jpfContrasenya = new JPasswordField();
        jpfContrasenya.setHorizontalAlignment(JTextField.LEFT);
        jpfContrasenya.setAlignmentX(Component.CENTER_ALIGNMENT);
        jpfContrasenya.setPreferredSize(new Dimension(180, 20));

        //JLabel de confirmar contrasenya
        jlConfirmar = new JLabel("Confirmar contraseña");
        jlConfirmar.setHorizontalAlignment(JLabel.CENTER);

        //JPasswordField de confirmar contrasenya
        jpfConfirmar = new JPasswordField();
        jpfConfirmar.setHorizontalAlignment(JTextField.LEFT);
        jpfConfirmar.setAlignmentX(Component.RIGHT_ALIGNMENT);
        jpfConfirmar.setPreferredSize(new Dimension(180, 20));

        jlRestriccions1 = new JLabel("                                      ");
        jlRestriccions2 = new JLabel("1. Usuari: Ha de ser únic");
        jlRestriccions3 = new JLabel("2. Correu: Ha de ser correcte i únic");
        jlRestriccions4 = new JLabel("3. Contrasenya: Ha de contenir com a mínim 6 caràcters, amb minúscules, majúscules i valors numèrics");
        jlRestriccions5 = new JLabel("4. Confirmar contrasenya: Ha de ser igual que la contrasenya");
        jlRestriccions6 = new JLabel("                                       ");

        jpRestriccions = new JPanel();
        jpRestriccions.setLayout(new GridLayout(5, 1));
        //jpRestriccions.add(jlRestriccions1);
        jpRestriccions.add(jlRestriccions2);
        jpRestriccions.add(jlRestriccions3);
        jpRestriccions.add(jlRestriccions4);
        jpRestriccions.add(jlRestriccions5);
        jpRestriccions.add(jlRestriccions6);
        jpRestriccions.setBackground(Color.white);

        jbRegistre = new JButton("Registra't");
        jbRegistre.setHorizontalAlignment(JLabel.CENTER);
        jbRegistre.setAlignmentX(Component.CENTER_ALIGNMENT);


        jbCancela = new JButton("Cancela");

        jpBotons = new JPanel();
        jpBotons.add(jbRegistre);
        jpBotons.add(jbCancela);
        jpBotons.setBackground(Color.white);


        jpCenter1 = new JPanel();
        jpCenter2 = new JPanel();
        jpCenter3 = new JPanel();
        jpCenter4 = new JPanel();
        jpCenter5 = new JPanel();
        jpCenter6 = new JPanel();

        ((FlowLayout)jpCenter1.getLayout()).setAlignment(FlowLayout.CENTER);
        ((FlowLayout)jpCenter2.getLayout()).setAlignment(FlowLayout.CENTER);
        ((FlowLayout)jpCenter3.getLayout()).setAlignment(FlowLayout.CENTER);
        ((FlowLayout)jpCenter4.getLayout()).setAlignment(FlowLayout.CENTER);
        ((FlowLayout)jpCenter5.getLayout()).setAlignment(FlowLayout.CENTER);
        ((FlowLayout)jpCenter6.getLayout()).setAlignment(FlowLayout.CENTER);

        jpCenter1.add(jlUsuari);
        jpCenter1.add(jtfUsuari);
        jpCenter1.setBackground(Color.white);

        jpCenter5.add(jlNom);
        jpCenter5.add(jtfNom);
        jpCenter5.setBackground(Color.white);

        jpCenter6.add(jlApellido);
        jpCenter6.add(jtfApellido);
        jpCenter6.setBackground(Color.white);

        jpCenter2.add(jlCorreu);
        jpCenter2.add(jtfCorreu);
        jpCenter2.setBackground(Color.white);

        jpCenter3.add(jlContrasenya);
        jpCenter3.add(jpfContrasenya);
        jpCenter3.setBackground(Color.white);

        jpCenter4.add(jlConfirmar);
        jpCenter4.add(jpfConfirmar);
        jpCenter4.setBackground(Color.white);



        jpCenter = new JPanel();
        jpCenter.setLayout(new BoxLayout(jpCenter, BoxLayout.Y_AXIS));
        jpCenter.add(jpRestriccions);
        jpCenter.add(jpCenter1);
        jpCenter.add(jpCenter5);
        jpCenter.add(jpCenter6);
        jpCenter.add(jpCenter2);
        jpCenter.add(jpCenter3);
        jpCenter.add(jpCenter4);
        jpCenter.add(jpBotons);
        jpCenter.setBackground(Color.white);



        this.add(jlEtiqueta, BorderLayout.PAGE_START);
        this.add(jpCenter);
        this.setBackground(Color.white);




    }

    public JLabel getJlUsuari() {
        return jlUsuari;
    }

    public void setJlUsuari(JLabel jlUsuari) {
        this.jlUsuari = jlUsuari;
    }

    public JLabel getJlEtiqueta() {
        return jlEtiqueta;
    }

    public void setJlEtiqueta(JLabel jlEtiqueta) {
        this.jlEtiqueta = jlEtiqueta;
    }

    public  JButton getJbRegistre() {
        return jbRegistre;
    }

    public void setJbRegistre(JButton jbRegistre) {
        this.jbRegistre = jbRegistre;
    }

    public JTextField getJtfUsuari() {
        return jtfUsuari;
    }

    public void setJtfUsuari(JTextField jtfUsuari) {
        this.jtfUsuari = jtfUsuari;
    }

    public JTextField getJtfCorreu() {
        return jtfCorreu;
    }

    public void setJtfCorreu(JTextField jtfCorreu) {
        this.jtfCorreu = jtfCorreu;
    }

    public JPasswordField getJpfContrasenya() {
        return jpfContrasenya;
    }

    public JTextField getJtfNom() {return jtfNom;}

    public JTextField getJtfApellido() {return jtfApellido; }

    public void setJpfContrasenya(JPasswordField jpfContrasenya) {
        this.jpfContrasenya = jpfContrasenya;
    }

    public JPasswordField getJpfConfirmar() {
        return jpfConfirmar;
    }

    public void setJpfConfirmar(JPasswordField jpfConfirmar) {
        this.jpfConfirmar = jpfConfirmar;
    }

    public JLabel getJlCorreu() {
        return jlCorreu;
    }

    public void setJlCorreu(JLabel jlCorreu) {
        this.jlCorreu = jlCorreu;
    }

    public JLabel getJlContrasenya() {
        return jlContrasenya;
    }

    public void setJlContrasenya(JLabel jlContrasenya) {
        this.jlContrasenya = jlContrasenya;
    }

    public JLabel getJlConfirmar() {
        return jlConfirmar;
    }

    public void setJlConfirmar(JLabel jlConfirmar) {
        this.jlConfirmar = jlConfirmar;
    }

    public JPanel getJpCenter() {
        return jpCenter;
    }

    public void setJpCenter(JPanel jpCenter) {
        this.jpCenter = jpCenter;
    }

    public JPanel getJpCenter1() {
        return jpCenter1;
    }

    public void setJpCenter1(JPanel jpCenter1) {
        this.jpCenter1 = jpCenter1;
    }

    public JPanel getJpCenter2() {
        return jpCenter2;
    }

    public void setJpCenter2(JPanel jpCenter2) {
        this.jpCenter2 = jpCenter2;
    }

    public JPanel getJpCenter3() {
        return jpCenter3;
    }

    public void setJpCenter3(JPanel jpCenter3) {
        this.jpCenter3 = jpCenter3;
    }

    public JPanel getJpCenter4() {
        return jpCenter4;
    }

    public void setJpCenter4(JPanel jpCenter4) {
        this.jpCenter4 = jpCenter4;
    }

    public JButton getJbCancela() {
        return jbCancela;
    }

    public void setJbCancela(JButton jbCancela) {
        this.jbCancela = jbCancela;
    }

    public JLabel getJlRestriccions1() {
        return jlRestriccions1;
    }

    public void setJlRestriccions1(JLabel jlRestriccions1) {
        this.jlRestriccions1 = jlRestriccions1;
    }

    public JLabel getJlRestriccions2() {
        return jlRestriccions2;
    }

    public void setJlRestriccions2(JLabel jlRestriccions2) {
        this.jlRestriccions2 = jlRestriccions2;
    }

    public JLabel getJlRestriccions3() {
        return jlRestriccions3;
    }

    public void setJlRestriccions3(JLabel jlRestriccions3) {
        this.jlRestriccions3 = jlRestriccions3;
    }

    public JLabel getJlRestriccions4() {
        return jlRestriccions4;
    }

    public void setJlRestriccions4(JLabel jlRestriccions4) {
        this.jlRestriccions4 = jlRestriccions4;
    }

    public JLabel getJlRestriccions5() {
        return jlRestriccions5;
    }

    public void setJlRestriccions5(JLabel jlRestriccions5) {
        this.jlRestriccions5 = jlRestriccions5;
    }

    public JPanel getJpBotons() {
        return jpBotons;
    }

    public void setJpBotons(JPanel jpBotons) {
        this.jpBotons = jpBotons;
    }

    public JPanel getJpRestriccions() {
        return jpRestriccions;
    }

    public void setJpRestriccions(JPanel jpRestriccions) {
        this.jpRestriccions = jpRestriccions;
    }
}
