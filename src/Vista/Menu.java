package Vista;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Uri on 1/4/2017.
 */
public class Menu extends JPanel {
    private JLabel jlTitol;
    private JButton jbVisvols;
    private JButton jbVisAeroports;
    private JButton jbVeureVols;
    private JPanel jpCentre;
    private JButton jbAccedir;
    private JButton jbRegistrarse;
    private JLabel jlEspai1;
    private JLabel jlEspai2;


    public Menu() {

        //Titol de la pagina principal
        jlTitol = new JLabel(new ImageIcon("Imatges/titol.png"));
        jlTitol.setHorizontalAlignment(JLabel.CENTER);
        jlTitol.setAlignmentX(Component.CENTER_ALIGNMENT);
        jlTitol.setBackground(Color.white);


        //Panell central que contindra els botons de les opcions del menu
        jpCentre = new JPanel();
        jpCentre.setLayout(new BoxLayout(jpCentre, BoxLayout.PAGE_AXIS));
        jpCentre.setBackground(Color.white);

        jbAccedir = new JButton("Login");
        jbAccedir.setForeground(Color.white);
        jbAccedir.setPreferredSize(new Dimension(1000, 100));
        jbAccedir.setIcon(new ImageIcon("Imatges/Login.png"));
        jbAccedir.setAlignmentX(Component.CENTER_ALIGNMENT);
        jbAccedir.setBorderPainted(false);
        jbAccedir.setFocusPainted(false);
        jbAccedir.setBackground(Color.white);
        jbAccedir.setRolloverIcon(new ImageIcon("Imatges/Login2.png"));
        jbAccedir.setPressedIcon(new ImageIcon("Imatges/Login2.png"));
        jbAccedir.setFont(new Font("Arial", Font.PLAIN, 0));

        jlEspai1 = new JLabel("                          ");

        jbRegistrarse = new JButton("Registrar-se");
        jbRegistrarse.setForeground(Color.white);
        jbRegistrarse.setPreferredSize(new Dimension(1000, 100));
        jbRegistrarse.setIcon(new ImageIcon("Imatges/Registrar-se.png"));
        jbRegistrarse.setAlignmentX(Component.CENTER_ALIGNMENT);
        jbRegistrarse.setBorderPainted(false);
        jbRegistrarse.setFocusPainted(false);
        jbRegistrarse.setBackground(Color.white);
        jbRegistrarse.setRolloverIcon(new ImageIcon("Imatges/Registrar-se2.png"));
        jbRegistrarse.setPressedIcon(new ImageIcon("Imatges/Registrar-se2.png"));
        jbRegistrarse.setFont(new Font("Arial", Font.PLAIN, 0));


       /* jbVisvols = new JButton("Comprar i visualitzar bitllets");
        jbVisvols.setAlignmentX(Component.CENTER_ALIGNMENT);

        jlEspai1 = new JLabel("                     ");
        jlEspai2 = new JLabel("                     ");


        jbVisAeroports = new JButton("Consultar aeroports");
        jbVisAeroports.setAlignmentX(Component.CENTER_ALIGNMENT);

        jbVeureVols = new JButton("Veure estat dels vols");
        jbVeureVols.setAlignmentX(Component.CENTER_ALIGNMENT);
*/
        jpCentre.add(jbAccedir);
  //      jpCentre.add(jbVisvols);
        jpCentre.add(jlEspai1);
        jpCentre.add(jbRegistrarse);
    //    jpCentre.add(jbVisAeroports);
      //  jpCentre.add(jlEspai2);
        //jpCentre.add(jbVeureVols);

        this.setLayout(new BorderLayout());
        this.setBackground(Color.white);
        this.add(jlTitol, BorderLayout.PAGE_START);
        this.add(jpCentre, BorderLayout.CENTER);




    }

    public JLabel getJlTitol() {
        return jlTitol;
    }

    public void setJlTitol(JLabel jlTitol) {
        this.jlTitol = jlTitol;
    }

    public JButton getJbVisvols() {
        return jbVisvols;
    }

    public void setJbVisvols(JButton jbVisvols) {
        this.jbVisvols = jbVisvols;
    }

      public JButton getJbVisAeroports() {
        return jbVisAeroports;
    }

    public void setJbVisAeroports(JButton jbVisAeroports) {
        this.jbVisAeroports = jbVisAeroports;
    }

    public JButton getJbVeureVols() {
        return jbVeureVols;
    }

    public void setJbVeureVols(JButton jbVeureVols) {
        this.jbVeureVols = jbVeureVols;
    }

    public JButton getJbAccedir() {
        return jbAccedir;
    }

    public void setJbAccedir(JButton jbAccedir) {
        this.jbAccedir = jbAccedir;
    }

    public JPanel getJpCentre() {
        return jpCentre;
    }

    public void setJpCentre(JPanel jpCentre) {
        this.jpCentre = jpCentre;
    }

    public JLabel getJlEspai1() {
        return jlEspai1;
    }

    public void setJlEspai1(JLabel jlEspai1) {
        this.jlEspai1 = jlEspai1;
    }

    public JLabel getJlEspai2() {
        return jlEspai2;
    }

    public void setJlEspai2(JLabel jlEspai2) {
        this.jlEspai2 = jlEspai2;
    }

    public JButton getJbRegistrarse() {
        return jbRegistrarse;
    }

    public void setJbRegistrarse(JButton jbRegistrarse) {
        this.jbRegistrarse = jbRegistrarse;
    }
}


