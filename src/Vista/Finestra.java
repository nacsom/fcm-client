package Vista;

import Controlador.ControladorBoto;
import Model.Billete;
import Model.Vuelo;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;

/**
 * Created by Uri on 14/4/2017.
 */
public class Finestra extends JFrame{

    private Menu menu;
    private Menu2 menu2;
    private Registrarse registrarse;
    private Login login;
    private CardLayout layout;
    private ControladorBoto controlador;
    private Aeroport aeroport;
    private CompraBitllet compraBitllet;
    private estatVols estatVols;

    public Finestra() {

        menu = new Menu();
        menu2 = new Menu2();
        registrarse = new Registrarse();
        login = new Login();
        layout = new CardLayout();
        aeroport = new Aeroport();
        compraBitllet = new CompraBitllet();
        estatVols = new estatVols();

        this.getContentPane().setLayout(layout);
        this.getContentPane().add(menu, "MENU");
        this.getContentPane().add(menu2, "MENU2");
        this.getContentPane().add(login, "LOGIN");
        this.getContentPane().add(registrarse, "REGISTRARSE");
        this.getContentPane().add(aeroport, "AEROPORT");
        this.getContentPane().add(compraBitllet, "COMPRA_BITLLET");
        this.getContentPane().add(estatVols,"ESTAT_VOLS");

        layout.show(this.getContentPane(), "MENU");

        setSize(825, 500);
        setTitle("FCM");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }

        public void posaControlador (ControladorBoto controlador){
            this.controlador = controlador;
        }

        public void vesLogin(){
            this.setSize(825, 500);
            this.getLogin().getJtfUsuari().setText(null);
            this.getLogin().getJpfPassword().setText(null);
            layout.show(this.getContentPane(), "LOGIN");
            setLocationRelativeTo(null);
        }

        public void vesRegistrarse(){
            this.setSize(825, 500);
            this.getRegistrarse().getJtfUsuari().setText(null);
            this.getRegistrarse().getJtfCorreu().setText(null);
            this.getRegistrarse().getJtfApellido().setText(null);
            this.getRegistrarse().getJtfNom().setText(null);
            this.getRegistrarse().getJpfContrasenya().setText(null);
            this.getRegistrarse().getJpfConfirmar().setText(null);
            layout.show(this.getContentPane(), "REGISTRARSE");
            setLocationRelativeTo(null);
        }

        public void vesErrorRegistre(){
            this.setSize(825, 500);

        }
        public void vesErrorLogin(){
            this.setSize(825, 500);

        }

        public void vesMenu(){
            this.setSize(825, 500);
            layout.show(this.getContentPane(), "MENU");
            setLocationRelativeTo(null);
        }

        public void vesMenu2(){
            this.setSize(825, 500);
            layout.show(this.getContentPane(), "MENU2");
            setLocationRelativeTo(null);
        }

        public void vesAeroport(){
            this.setSize(825, 500);
            layout.show(this.getContentPane(), "AEROPORT");
            setLocationRelativeTo(null);
        }

        public void vesCompraBitllet(){
            this.setSize(825, 500);
            layout.show(this.getContentPane(), "COMPRA_BITLLET");
            setLocationRelativeTo(null);
        }

        public void vesEstatVols(){
            this.setSize(825, 500);
            layout.show(this.getContentPane(), "ESTAT_VOLS");
            setLocationRelativeTo(null);
        }

        //procediment que comprova el registre i genera el missatge d'error de la classe ErrorRegistre
        public boolean comprovaRegistre(){
            boolean upCase = false;
            boolean loCase = false;
            boolean diCase = false;
            boolean totOk = false;
            if (this.getRegistrarse().getJtfUsuari().getText().equals("") || this.getRegistrarse().getJtfCorreu().getText().equals("") || this.getRegistrarse().getJpfContrasenya().getText().equals("") || this.getRegistrarse().getJpfConfirmar().getText().equals("") ||  this.getRegistrarse().getJtfApellido().getText().equals("") ||  this.getRegistrarse().getJtfNom().getText().equals("")){
            //    this.errorRegistre("No pots deixar cap camp buit!", 450, 275);
                JOptionPane.showMessageDialog(this,
                        "No pots deixar cap camp buit!",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            }else{
                if (this.getRegistrarse().getJpfContrasenya().getText().equals(this.getRegistrarse().getJpfConfirmar().getText())) {
                    if (this.getRegistrarse().getJpfContrasenya().getText().length() < 6) {
                        JOptionPane.showMessageDialog(this,
                                "La contrasenya ha de contenir com mínim 6 caràcters",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }else {
                        for (int i = 0; i < this.getRegistrarse().getJpfContrasenya().getText().length(); i++) {
                            if (Character.isUpperCase(this.getRegistrarse().getJpfContrasenya().getText().charAt(i))) {
                                upCase = true;
                            }
                            if (Character.isLowerCase(this.getRegistrarse().getJpfContrasenya().getText().charAt(i))) {
                                loCase = true;
                            }
                            if (Character.isDigit(this.getRegistrarse().getJpfContrasenya().getText().charAt(i))) {
                                diCase = true;
                            }
                            if (upCase && loCase && diCase) {
                                totOk = true;
                            }
                        }

                    }
                }else {
                   // this.errorRegistre("Contrasenya i Confirmar contrasenya han de ser idèntics", 450, 275);
                    JOptionPane.showMessageDialog(this,
                            "Contrasenya i Confirmar contrasenya han de ser idèntics",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                if(!(upCase)){
                    JOptionPane.showMessageDialog(this,
                            "La contraseña no contiene una mayúscula",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }else if(!loCase){
                    JOptionPane.showMessageDialog(this,
                            "La contraseña no contiene una minúscula",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }else if(!diCase){
                    JOptionPane.showMessageDialog(this,
                            "La contraseña no contiene un digito",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }else if(!totOk){
                    JOptionPane.showMessageDialog(this,
                            "La contraseña no es segura",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
            return totOk;
        }

        public void comprovaLogin(){
            if (this.getLogin().getJtfUsuari().getText().equals("") || this.getLogin().getJpfPassword().getText().equals("")){
                JOptionPane.showMessageDialog(this,
                        "No pots deixar cap camp buit!",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
        //mostra el panell d'ErrorRegistre quan l'usuari s'ha registrat malament

        public void consulta10(LinkedList<Vuelo> vuelos){
            aeroport.muestraVuelos(vuelos);
        }
        public void consultaBilletesUsuario(LinkedList<Billete> billetes){
            compraBitllet.muestraBilletesUsuario(billetes);
        }


        public void assingaControladorBoto(ControladorBoto controlador){

           registrarse.getJbRegistre().addActionListener(controlador);
           registrarse.getJbCancela().addActionListener(controlador);

           login.getJbCancela().addActionListener(controlador);
           login.getJbEntra().addActionListener(controlador);

            menu.getJbAccedir().addActionListener(controlador);
            menu.getJbRegistrarse().addActionListener(controlador);

            menu2.getJbVisAeroports().addActionListener(controlador);
            menu2.getJbVeureVols().addActionListener(controlador);
            menu2.getJbVisvols().addActionListener(controlador);
            menu2.getJbDesconectar().addActionListener(controlador);

           registrarse.getJbCancela().addActionListener(controlador);

           aeroport.getJbConsulta10().addActionListener(controlador);
            aeroport.getJbCancela().addActionListener(controlador);

           compraBitllet.getJbConsultaBilletes().addActionListener(controlador);
            compraBitllet.getJbCompraBillete().addActionListener(controlador);
            compraBitllet.getJbCancela().addActionListener(controlador);
            estatVols.getJbConsultaEstado().addActionListener(controlador);
            estatVols.getJbCancelarEstado().addActionListener(controlador);
            estatVols.getJbCancela().addActionListener(controlador);

        }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Registrarse getRegistrarse() {
        return registrarse;
    }

    public void setRegistrarse(Registrarse registrarse) {
        this.registrarse = registrarse;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public Aeroport getAeroport() {
        return aeroport;
    }

    public void setAeroport(Aeroport aeroport) {
        this.aeroport = aeroport;
    }

    @Override
    public CardLayout getLayout() {
        return layout;
    }

    public void setLayout(CardLayout layout) {
        this.layout = layout;
    }

    public ControladorBoto getControlador() {
        return controlador;
    }

    public void setControlador(ControladorBoto controlador) {
        this.controlador = controlador;
    }


    //FUNCIONS PER CAPTURAR TEXTOS DE LA INTERFÍCIE

    public String loginGetUsername(){
        return getLogin().getJtfUsuari().getText();
    }

    public String loginGetPassword(){
        return getLogin().getJpfPassword().getText();
    }

    public String registrarseGetUsername(){
        return getRegistrarse().getJtfUsuari().getText();
    }

    public String registrarseGetPassword(){
        return getRegistrarse().getJpfContrasenya().getText();
    }

    public String registrarseGetEmail(){
        return getRegistrarse().getJtfCorreu().getText();
    }

    public CompraBitllet getCompraBitllet() {
        return compraBitllet;
    }

    public void setCompraBitllet(CompraBitllet compraBitllet) {
        this.compraBitllet = compraBitllet;
    }

    public Menu2 getMenu2() {
        return menu2;
    }

    public void setMenu2(Menu2 menu2) {
        this.menu2 = menu2;
    }

    public Vista.estatVols getEstatVols() {
        return estatVols;
    }

    public void setEstatVols(Vista.estatVols estatVols) {
        this.estatVols = estatVols;
    }

}




