package Vista;

import Model.Vuelo;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.LinkedList;

/**
 * Created by Sefa on 24/4/17.
 */
public class Aeroport extends JPanel {

    private JComboBox<String> jcbAeropuertos;   //PANELL 0
    private DefaultTableModel modelVuelos;
    private JButton jbConsulta10;
    private JTable jtVuelos;
    private JButton jbCancela;
    private boolean firstTime = true;


    public Aeroport(){
            this.setLayout(new BorderLayout());

            JPanel panel = new JPanel(new BorderLayout());
            Panel panelNorth = new Panel(new GridLayout(1,1));

            jcbAeropuertos = new JComboBox();

            panelNorth.add(jcbAeropuertos);

            jtVuelos = new JTable();
            jbConsulta10 = new JButton("Consulta 10");
            jbConsulta10.setActionCommand("CONSULTA_10");

            JScrollPane scrollPane = new JScrollPane(jtVuelos);
            panel.add(scrollPane, BorderLayout.CENTER);
        //    panel.add(jbConsulta10, BorderLayout.SOUTH);

            modelVuelos = new DefaultTableModel();
            jtVuelos = new JTable(modelVuelos);       //Creem JTable amb el seu model, afegim 3 columnes
            modelVuelos.addColumn("Identificador");
            modelVuelos.addColumn("Places disponibles");
            modelVuelos.addColumn("Data Sortida");
            modelVuelos.addRow(new Object[]{"Identificador", "Places Disponibles", "Data sortida"});

            panel.add(jtVuelos, BorderLayout.CENTER);
            panel.add(panelNorth, BorderLayout.NORTH);

            this.add(panel, BorderLayout.CENTER);

            JPanel panelAux = new JPanel(new GridLayout(2,0));
            jbCancela = new JButton("Cancela");
            jbCancela.setActionCommand("Cancela aeroports");
            panelAux.add(jbConsulta10);
            panelAux.add(jbCancela);

            panel.add(panelAux, BorderLayout.SOUTH);
    }

    public void muestraVuelos(LinkedList<Vuelo> vuelos){  //mostra els vols en una taula al panell
        modelVuelos.setRowCount(0); //Esborrem taula cada cop.
        modelVuelos.setColumnCount(0);
        modelVuelos.addColumn("Identificador");
        modelVuelos.addColumn("Places disponibles");
        modelVuelos.addColumn("Data Sortida");
        modelVuelos.addRow(new Object[]{"Identificador", "Places Disponibles", "Data sortida"});
        int numeroVuelos = vuelos.size();
        if(!(numeroVuelos == 0)) {
            for (int i = 0; i < numeroVuelos; i++) { //Mostrem tots els vols (primers 10 o menys)
                modelVuelos.addRow(new Object[]{vuelos.get(i).getReferencia(), vuelos.get(i).getPlazas(), vuelos.get(i).getFechaSalida()});
            }
        }else{
            JOptionPane.showMessageDialog(this,
                    "No hi han vols disponibles per aquest aeroport",
                    "AVIS",
                    JOptionPane.WARNING_MESSAGE);
        }
    }





    public JComboBox<String> getJcbAeropuertos() {
        return jcbAeropuertos;
    }

    public void setJcbAeropuertos(LinkedList<String> airportsName) {    //Pasamos LinkedList con nombres en vez de un JComboBox
        int numberOfAirports = airportsName.size();
        int numberOfComboBox = this.jcbAeropuertos.getItemCount();
        for(int i = 0; i < numberOfAirports; i++){
            boolean dontAdd = false;
            for(int j = 0; j < numberOfComboBox; j++){
                if(this.jcbAeropuertos.getItemAt(j).equals(airportsName.get(i))) dontAdd = true;
            }
            if(!dontAdd) this.jcbAeropuertos.addItem(airportsName.get(i));
        }
    }

    public DefaultTableModel getModelVuelos() {
        return modelVuelos;
    }

    public void setModelVuelos(DefaultTableModel modelVuelos) {
        this.modelVuelos = modelVuelos;
    }

    public JButton getJbConsulta10() {
        return jbConsulta10;
    }

    public void setJbConsulta10(JButton jbConsulta10) {
        this.jbConsulta10 = jbConsulta10;
    }

    public JTable getJtVuelos() {
        return jtVuelos;
    }

    public void setJtVuelos(JTable jtVuelos) {
        this.jtVuelos = jtVuelos;
    }

    public JButton getJbCancela() { return jbCancela; }
}
