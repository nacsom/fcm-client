package Network;

import Model.Billete;
import Model.Vuelo;
import Vista.Finestra;

import javax.swing.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.Date;
import java.util.LinkedList;

// Permet: (1) rebre les actualitzacions de lestat del model que
// emmagatzema el servidor i (2) enviar de les dades (fila, columna)
// quan lusuari selecciona una de les caselles de la graella.
public class ServerComunication extends Thread {
	private boolean isOn;
	private Socket socketToServer;
	private NetworkConfiguration networkConfiguration;
	//private Socket socketToUpdate;
	private Finestra view;

	private DataOutputStream dataOut;
	private DataInputStream dataIn;
	private ObjectInputStream objectIn;

	private LinkedList<Billete> billetes;

	public ServerComunication(Finestra view) {
		try {
			this.isOn = false;
			this.view = view;
			// connectem amb el servidor i obrim els canals de comunicacio
			this.networkConfiguration = new NetworkConfiguration();
			this.socketToServer = new Socket(networkConfiguration.getSERVER_IP(), networkConfiguration.getPORT_CLIENT());

			this.dataOut = new DataOutputStream(socketToServer.getOutputStream());
			this.dataIn = new DataInputStream(socketToServer.getInputStream());

			this.objectIn = new ObjectInputStream(socketToServer.getInputStream());

			this.billetes = new LinkedList();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("*** ESTA EL SERVIDOR EN EXECUCIO? ***");
		}
	}

	public void run() {
		while (isOn) {
			//BUCLE ABSURD (no fa res)
			/*
			[PROTOCOL EXPLICAT AMB DETALL AL SERVIDOR (updateServer class)]
			 */
		}
		stopServerComunication();
	}

	public void startServerComunication() {
		// iniciem la comunicacio amb el servidor
		isOn = true;
		this.start();
	}

	public void stopServerComunication() {
		// aturem la comunicacio amb el servidor
		this.isOn = false;
		this.interrupt();
	}

	public boolean wantToLogin() {
		try {
			dataOut.writeUTF("WANT_TO_LOGIN");	//Informem al server amb aquesta instrucció
			dataOut.writeUTF(view.loginGetUsername());
			dataOut.writeUTF(view.loginGetPassword());	//Enviem juntament login i password
			String result = dataIn.readUTF();
			if(result.equals("OK")){
				view.getLogin().setUsername(view.getLogin().getJtfUsuari().getText());	//Ha loggejat correctament, guardem el username a la classe Login.
				JOptionPane.showMessageDialog(view,
						"Has loggejat correctament, " + view.getLogin().getUsername() + "!",
						"Benvingut",
						JOptionPane.WARNING_MESSAGE);
				return true;
			}else{
				JOptionPane.showMessageDialog(view,
						"Error loggejant com a " + view.loginGetUsername(),
						"Alguna cosa ha anat malament...",
						JOptionPane.ERROR_MESSAGE);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public void wantToRegister(){
		try {
			dataOut.writeUTF("WANT_TO_REGISTER");
			dataOut.writeUTF(view.getRegistrarse().getJtfUsuari().getText());
			dataOut.writeUTF(view.getRegistrarse().getJpfContrasenya().getText());	//Enviem juntament login, password i email
			dataOut.writeUTF(view.getRegistrarse().getJtfCorreu().getText());
			dataOut.writeUTF(view.getRegistrarse().getJtfNom().getText());
			dataOut.writeUTF(view.getRegistrarse().getJtfApellido().getText());
			String status = dataIn.readUTF();
			if(status.equals("OK")){
				JOptionPane.showMessageDialog(view,
						"S'ha enregistrat correctament l'usuari " + view.getRegistrarse().getJtfUsuari().getText() + "!",
						"Registra't amb èxit",
						JOptionPane.WARNING_MESSAGE);
			}else{
				JOptionPane.showMessageDialog(view,
						"Aquest nom d'usuari està ocupat",
						"Alguna cosa ha anat malament...",
						JOptionPane.ERROR_MESSAGE);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public LinkedList<String> airportGetAvailableAirports() throws IOException, ClassNotFoundException {
		LinkedList<String> airportsName = new LinkedList<String>();
		dataOut.writeUTF("WANT_AVAILABLE_AIRPORTS");
		airportsName = (LinkedList<String>) objectIn.readObject();

		return airportsName;
	}

	public LinkedList<Vuelo> want10Flights(){
		LinkedList<Vuelo> vuelos = new LinkedList<Vuelo>();
		try {
			dataOut.writeUTF("WANT_10_FLIGHTS");
			String airportName = (String) view.getAeroport().getJcbAeropuertos().getSelectedItem();
			dataOut.writeUTF(airportName);	//Enviem el nom de l'aeroport
			int numberOfFlights;
			numberOfFlights = dataIn.readInt();
			for(int i = 0; i < numberOfFlights; i++){
				String identificador = dataIn.readUTF();
				int plazasDisponibles = dataIn.readInt();
				String dataSortida = dataIn.readUTF();

				Vuelo vuelo = new Vuelo(identificador, dataSortida, 0, 0, "", "", "", "", plazasDisponibles);
				vuelos.add(vuelo);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return vuelos;
	}

	public LinkedList<Billete> wantBilletesUser(){
		try {
			this.billetes.clear();
			dataOut.writeUTF("WANT_BILLETES_USER");
			dataOut.writeUTF(view.getLogin().getJtfUsuari().getText());	//Enviem el nom d'usuari
			int numberOfBilletes = dataIn.readInt();
			for(int i = 0; i < numberOfBilletes; i++){
				String referencia = dataIn.readUTF();
				String referenciaUsuario = dataIn.readUTF();
				String referenciaVuelo = dataIn.readUTF();
				Billete billete = new Billete(referencia, referenciaUsuario, referenciaVuelo);
				this.billetes.add(billete);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return this.billetes;
	}

	public Vuelo wantShowFlightStatus(){
		Vuelo vuelo = null;
		String referenciaVuelo = null;
		int numberOfBilletes = this.billetes.size();
		try {
			for(int i = 0; i < numberOfBilletes; i++){
				if(view.getEstatVols().getJtVuelosComprados().getValueAt(view.getEstatVols().getJtVuelosComprados().getSelectedRow(), view.getEstatVols().getJtVuelosComprados().getSelectedColumn()).
						equals(this.billetes.get(i).getReferencia())){
					referenciaVuelo = this.billetes.get(i).getReferenciaVuelo();
					break;
				}
			}
			dataOut.writeUTF("WANT_SHOW_FLIGHT_STATUS");
			dataOut.writeUTF(referenciaVuelo);
			String referencia = dataIn.readUTF();
			String fechaSalida = dataIn.readUTF();
			int duracion = dataIn.readInt();
			int retraso = dataIn.readInt();
			String estado = dataIn.readUTF();
			String referenciaAvion = dataIn.readUTF();
			String referenciaOrigen = dataIn.readUTF();
			String referenciaDestino = dataIn.readUTF();
			vuelo = new Vuelo(referencia, fechaSalida, duracion, retraso, estado, referenciaAvion, referenciaOrigen, referenciaDestino, 0);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return vuelo;
	}

	public void wantToDisconnect(){
		try {
			dataOut.writeUTF("WANT_TO_DISCONNECT");
			socketToServer.close();
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public LinkedList<Vuelo> wantToBuyBilleteShowThem(Object aeropuertoOrigen, Object aeropuertoDestino, Date date) {
		try {
			dataOut.writeUTF("WANT_TO_BUY_BILLETE");
			dataOut.writeUTF(aeropuertoOrigen.toString());
			dataOut.writeUTF(aeropuertoDestino.toString());
			java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			dataOut.writeUTF(sdf.format(date));
			LinkedList<Vuelo> vuelos = readVuelos();
			return vuelos;
			//System.out.println("Size: " + vuelos.size());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public LinkedList<Vuelo> readVuelos(){
		LinkedList<Vuelo> vuelos = new LinkedList();
		try {
			int size = dataIn.readInt();
			for(int i = 0; i < size; i++){
				String referencia = dataIn.readUTF();
				String fechaSalida = dataIn.readUTF();
				int duracion = dataIn.readInt();
				int retraso = dataIn.readInt();
				String estado = dataIn.readUTF();
				String referenciaAvion = dataIn.readUTF();
				String referenciaOrigen = dataIn.readUTF();
				String getReferenciaDestina = dataIn.readUTF();
				int plazas = dataIn.readInt();
				vuelos.add(new Vuelo(referencia, fechaSalida, duracion, retraso, estado, referenciaAvion, referenciaOrigen, getReferenciaDestina, plazas));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return vuelos;
	}

	public String wantToBuyBillete(String referencia, String username, int i){
		try {
			dataOut.writeUTF("PETICIO_BUY_BILLETE");
			dataOut.writeUTF(referencia);
			dataOut.writeUTF(username);
			dataOut.writeInt(i);
			return dataIn.readUTF(); 	//Retornem la resposta
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
