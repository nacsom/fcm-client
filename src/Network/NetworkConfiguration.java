package Network;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class NetworkConfiguration {
	// constants relacionades amb la comunicacio

	private String  SERVER_IP;
	private String PORT_CLIENT;
	private String PORT_AVION;

	public NetworkConfiguration(){
		ArrayList<String> arrayList = readFile();
		this.SERVER_IP = arrayList.get(0);
		this.PORT_CLIENT = arrayList.get(1);
		this.PORT_AVION = arrayList.get(2);
	}

	public ArrayList<String> readFile(){
		Gson gson = new Gson();
		try {
			BufferedReader br = new BufferedReader(new FileReader("libs/config.json"));
			ArrayList<String> arrayList;
			arrayList = gson.fromJson(br, ArrayList.class);
			return arrayList;
		}catch (IOException e){
			e.printStackTrace();
		}
		return null;
	}

	public String getSERVER_IP() {
		return SERVER_IP;
	}

	public void setSERVER_IP(String SERVER_IP) {
		this.SERVER_IP = SERVER_IP;
	}

	public int getPORT_CLIENT() {
		return Integer.valueOf(PORT_CLIENT);
	}

	public void setPORT_CLIENT(String PORT_CLIENT) {
		this.PORT_CLIENT = PORT_CLIENT;
	}

	public int getPORT_AVION() {
		return Integer.parseInt(PORT_AVION);
	}

	public void setPORT_AVION(String PORT_AVION) {
		this.PORT_AVION = PORT_AVION;
	}
}
