package Model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Clase Modelo Vuelo
 */
public class Vuelo implements Serializable {

    static final long serialVersionUID = 40L;
    private String referencia;
    private String fechaSalida;
    private int duracion;
    private int retraso;
    private String estado;
    private String referenciaAvion;
    private String referenciaOrigen;
    private String referenciaDestino;
    private int plazas;
    private String modeloAvion;
    private int pincode;
    private int loginFlag;
    public Vuelo(){}

    public Vuelo(String referencia, String fechaSalida, int duracion, int retraso, String estado,
                 String referenciaAvion, String referenciaOrigen, String referenciaDestino, int plazas) {
        this.referencia = referencia;
        this.fechaSalida = fechaSalida;
        this.duracion = duracion;
        this.retraso = retraso;
        this.estado = estado;
        this.referenciaAvion = referenciaAvion;
        this.referenciaOrigen = referenciaOrigen;
        this.referenciaDestino = referenciaDestino;
        this.plazas = plazas;
    }

    private void writeObject(ObjectOutputStream stream){
        try {
            stream.writeObject(referencia);
            stream.writeObject(fechaSalida);
            stream.writeInt(duracion);
            stream.writeInt(retraso);
            stream.writeObject(estado);
            stream.writeObject(referenciaAvion);
            stream.writeObject(referenciaOrigen);
            stream.writeObject(referenciaDestino);
            stream.writeInt(plazas);
            stream.writeInt(loginFlag);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void readObject(ObjectInputStream stream){
        try {
            this.referencia = (String) stream.readObject();
            this.fechaSalida = (String) stream.readObject();
            this.duracion = stream.readInt();
            this.retraso = stream.readInt();
            this.estado = (String) stream.readObject();
            this.referenciaAvion = (String) stream.readObject();
            this.referenciaOrigen = (String) stream.readObject();
            this.referenciaDestino = (String) stream.readObject();
            this.plazas = stream.readInt();
            this.loginFlag = stream.readInt();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public int getRetraso() {
        return retraso;
    }

    public void setRetraso(int retraso) {
        this.retraso = retraso;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getReferenciaAvion() {
        return referenciaAvion;
    }

    public void setReferenciaAvion(String referenciaAvion) {
        this.referenciaAvion = referenciaAvion;
    }

    public String getReferenciaOrigen() {
        return referenciaOrigen;
    }

    public void setReferenciaOrigen(String referenciaOrigen) {
        this.referenciaOrigen = referenciaOrigen;
    }

    public String getGetReferenciaDestina() {
        return referenciaDestino;
    }

    public void setGetReferenciaDestina(String getReferenciaDestina) {
        this.referenciaDestino = getReferenciaDestina;
    }

    public int getPlazas() {
        return plazas;
    }

    public void setPlazas(int plazas) {
        this.plazas = plazas;
    }
}
