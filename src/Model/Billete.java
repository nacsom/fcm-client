package Model;

import java.io.Serializable;

/**
 * Clase Modelo Billete
 */
public class Billete implements Serializable{

    private String referencia;
    private String referenciaUsuario;
    private String referenciaVuelo;

    public Billete(){}
    public Billete(String referencia, String referenciaUsuario, String referenciaVuelo) {
        this.referencia = referencia;
        this.referenciaUsuario = referenciaUsuario;
        this.referenciaVuelo = referenciaVuelo;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getReferenciaUsuario() {
        return referenciaUsuario;
    }

    public void setReferenciaUsuario(String referenciaUsuario) {
        this.referenciaUsuario = referenciaUsuario;
    }

    public String getReferenciaVuelo() {
        return referenciaVuelo;
    }

    public void setReferenciaVuelo(String referenciaVuelo) {
        this.referenciaVuelo = referenciaVuelo;
    }
}
